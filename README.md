# README #

Current Version: 0.1.0 **[Alpha Prototype]**

**Product Description**
   Pulls images from e621 based off a tag search given by the user (supplied in imagebot.sh) and saves them in an image buffer. Conky is set to read from this buffer without cache.

**Current Function**
 - Pulls images from e621 and displays them to a constrained maximum size.
 
**Future Designs**
 - Add ability to log into e621 so searches can be filtered though blacklist
 - Various UX / UI improvements.
 - Offline support?

**Known Bugs**
 - Aspect ratio distortion, seems to be a problem with e621s metatagging. Investigating.